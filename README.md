[sujet](https://tinyurl.com/mlops-paper)


L'application heroku se trouve [ici](https://tp-mlops.herokuapp.com/).

### Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?

Apres avoir build l'image docker, on peut récupérer sa taille avec la commande ``docker images``. La taille de l'image docker obtenue est 977.63MB. En utilisant la commande ``docker stats``, la RAM utilisée monte jusqu'à environ 400MB. On peut donc définit une limite de RAM à 512MB.


###  Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99

Si le programme dépasse les 1500 utilisateurs avec 973 requêtes par seconde sur locust on obtient un P99 supérieur à 200ms. Il commence aussi à y avoir des erreurs dans les requêles lorsque l'on dépasse les 1000 utilisateurs.

<img src="./statistics/locust.PNG" width="1000">

Les statistiques de l'exemple ci-dessus sont disponibles [ici](./statistics/requests.csv).


### On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs ?

Il est possible de traiter les données en les regroupement par lots, ce qui est équivalent au fonctionnement d'un batch sur un modèle de machine learning.