FROM python:3.7-buster

ENV PYTHONUNBUFFERED 1
RUN mkdir -p app
WORKDIR app
COPY . ./
RUN pip install poetry
CMD sh app/entrypoint.sh $PORT
