import os

import uvicorn
import logging
from json import dumps

import spacy
from fastapi import FastAPI
import argparse

app = FastAPI()

logging.info("Loading model..")
nlp = spacy.load("app/models")


@app.get("/api/intent")
async def intent_inference(sentence: str):
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return dumps(inference.cats)


@app.get("/api/intent-supported-languages")
async def supported_languages():
    return dumps(["fr-FR"])


@app.get("/health", status_code=200)
async def health_check_endpoint():
    return


def start():
    parser = argparse.ArgumentParser(description='Run FastAPI application.')
    parser.add_argument('port', type=str, help='the port to expose the application on')

    args = parser.parse_args()
    port = int(args.port)
    uvicorn.run("app.main:app", host="0.0.0.0", port=int(os.environ.get('PORT', port)), reload=True)
