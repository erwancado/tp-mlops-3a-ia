from locust import HttpUser, between, task


class WebsiteUser(HttpUser):
    wait_time = between(5, 15)
    
    def on_start(self):
        pass
    
    @task
    def intent(self):
        self.client.get("/api/intent?sentence=Find%20a%20restaurant%20near%20me.")
        
    @task
    def languages(self):
        self.client.get("/api/intent-supported-languages")

    @task
    def health(self):
        self.client.get("/health")

'''
Usage:
uvicorn app.main:app --host=0.0.0.0 --port=${PORT:-5000}
locust -f locustfile.py -H http://localhost:5000
'''