import json

from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_intent_inference():
    response = client.get("/api/intent?sentence=Find%20a%20restaurant%20near%20me.")
    response_json = json.loads(response.json())
    assert response.status_code == 200
    assert len(response_json.keys()) == 8
    assert float(response_json["find-restaurant"]) > 0.5


def test_supported_languages():
    response = client.get("/api/intent-supported-languages")
    response_json = json.loads(response.json())
    assert response.status_code == 200
    assert response_json == ["fr-FR"]


def test_health_check_endpoint():
    response = client.get("/health")
    assert response.status_code == 200
